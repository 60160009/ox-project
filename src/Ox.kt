import java.util.*

object OX {
    var XO = arrayOf(
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " ")
    )
    var turn = "X"
    var R = 0
    var C = 0
    var Round = 0

    @JvmStatic
    fun main(args: Array<String>) {
        printStart()
        /// Start Game///
        while (true) {
            printBoard()

            /// Choose Row or Column///
            inputPosition()

            /// Check Win Check Draw ///
            if (checkWin()) {
                break
            } else if (checkDraw()) {
                break
            }

            // Swap Turn
            switchTurn()
        }
    }

    fun printStart() {
        println("Start Game OX")
    }

    fun printBoard() {
        println(" " + " 1 " + "2" + " 3 ")
        for (i in 0..2) {
            print(i + 1)
            for (j in 0..2) {
                print("|" + XO[i][j])
            }
            print("|")
            println()
        }
    }

    fun inputPosition() {
        val kb = Scanner(System.`in`)
        while (true) {
            println("Turn $turn")
            print("Plz Choose position(R,C) : ")
            try {
                val a = kb.next()
                val b = kb.next()
                R = a.toInt()
                C = b.toInt()
                if (R > 3 || R < 0 || R == 0 || C > 3 || C < 0 || C == 0) {
                    println("Row and Column must be number 1-3")
                    continue
                }
                R = R - 1
                C = C - 1
                if (XO[R][C] !== " ") {
                    println("Row " + (R + 1) + " and Column " + (C + 1) + " can't choose again")
                    continue
                }
                Round++
                XO[R][C] = turn
                break
            } catch (a: Exception) {
                println("Row and Column must be number")
                continue
            }
        }
    }

    fun checkWin(): Boolean {
        var chk = false
        // เช็คแนวนอน
        for (i in XO.indices) {
            if (XO[i][0] === turn && XO[i][1] === turn && XO[i][2] === turn
            ) {
                chk = true
            }
        }
        // เช็คแนวตั้ง
        for (i in XO.indices) {
            if (XO[0][i] === turn && XO[1][i] === turn && XO[2][i] === turn
            ) {
                chk = true
            }
        }
        // เช็คแนวแทยง
        if (XO[0][0] === turn && XO[1][1] === turn && XO[2][2] === turn
        ) {
            chk = true
        }
        if (XO[0][2] === turn && XO[1][1] === turn && XO[2][0] === turn
        ) {
            chk = true
        }
        if (chk == true) {
            printBoard()
            println("$turn WIN")
            return true
        }
        return false
    }

    fun checkDraw(): Boolean {
        if (Round == 9) {
            printBoard()
            println("DRAW")
            return true
        }
        return false
    }

    fun switchTurn() {
        turn = if (turn === "X") {
            "O"
        } else {
            "X"
        }
    }
}